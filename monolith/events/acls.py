from .keys import PEXELS_API_KEY as pkey, OPEN_WEATHER_API_KEY as okey
import requests
import json


def get_image(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}&{state}"
    headers = {"Authorization": pkey}
    response = requests.get(url, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content['photos'][0]['src']['original']}
    except:
        return {'picture_url': None}


def get_weather(city, state):
    # geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&appid={okey}"
    # lat_lon_response = requests.get(geocode_url)
    # parsed_lat_lon_response = json.loads(lat_lon_response.content)
    # lat_lon = {
    #     "lat": parsed_lat_lon_response[0]["lat"],
    #     "lon": parsed_lat_lon_response[0]["lon"]
    # }
    # weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat_lon['lat']}&lon={lat_lon['lon']}&units=imperial&appid={okey}"
    # weather_response = requests.get(weather_url)
    # parsed_weather_response = json.loads(weather_response.content)
    # current_weather = {
    #     "temp": parsed_weather_response["main"]["temp"],
    #     "description": parsed_weather_response["weather"][0]["description"]
    # }
    # return current_weather


    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&appid={okey}"
    response = requests.get(url)
    content = response.json()
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    url = f"https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": okey,
    }
    response = requests.get(url, params=params)
    content = response.json()
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    return {"description": description, "temp": temp}

    # redefine the parameters from here.
    # geo_response = requests.get(geo_api) #making the API request
    # geo_data = geo_response.json()
    # if geo_response.status_code != 200 or  geo_data.get("error"):
    #     print("you dun goofed with the geocoding")
    #     return None

    # lat = geo_data["coord"]["lat"]
    # long = geo_data["coord"]["lon"]

    # weather_api = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={long}&appid={okey}"
    # weather_response = requests.get(weather_api)
    # weather_data = weather_response.json()
    # if weather_response.status_code != 200 or weather_data.get("error"):
    #     print("it aint fine, weather sucks")
    #     return None

    # temp_k = weather_data["main"]["description"]
    # weather_description = weather_data["weather"]["description"]

    # temp_f = (temp_k - 273.15) * 9/5 + 32

    # weather_dict = {
    #    "temp_f": temp_f,
    #    "weather_description": weather_description
    # }

    # return weather_dict
