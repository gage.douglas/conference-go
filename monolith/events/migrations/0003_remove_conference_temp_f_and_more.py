# Generated by Django 4.0.3 on 2023-11-27 21:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_conference_temp_f_conference_weather_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='temp_f',
        ),
        migrations.RemoveField(
            model_name='conference',
            name='weather_description',
        ),
    ]
